# README #

Microservicio para obtener el numero de fibonacci

### Arrancar la aplicacion ###

* JDK 1.8
* mvn clean package
* java -jar target/fibonacci.jar

### Stack ###

* jdk 1.8
* SpringBoot
* Junit4
* Mockito

### application ###

* url: http://localhost:8080/fibonacciWS/api/v1/fibonacciCalculator?position=20
* url: http://localhost:8080/fibonacciWS/api/v2/fibonacciCalculator?position=20
* Documentacion: http://localhost:8080/fibonacciWS/swagger-ui.html
