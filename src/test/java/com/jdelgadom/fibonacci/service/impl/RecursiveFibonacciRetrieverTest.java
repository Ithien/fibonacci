package com.jdelgadom.fibonacci.service.impl;

import com.jdelgadom.fibonacci.common.exception.FibonacciException;
import com.jdelgadom.fibonacci.service.FibonacciRetriever;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RecursiveFibonacciRetrieverTest {

  FibonacciRetriever fibonacciRetriever;

  @Before
  public void setUp() {
    fibonacciRetriever = new RecursiveFibonacciRetriever();
  }


  @Test
  public void fibonacciCalculator() {

    assertEquals(Long.parseLong("0"), fibonacciRetriever.fibonacciCalculator(Long.parseLong("0")));
    assertEquals(Long.parseLong("1"), fibonacciRetriever.fibonacciCalculator(Long.parseLong("1")));
    assertEquals(Long.parseLong("8"), fibonacciRetriever.fibonacciCalculator(Long.parseLong("6")));

  }

  @Test(expected = FibonacciException.class)
  public void fibonacciCalculatorException() {
    fibonacciRetriever.fibonacciCalculator(Long.parseLong("-2"));
  }

}