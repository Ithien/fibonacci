package com.jdelgadom.fibonacci.api;

import com.jdelgadom.fibonacci.common.exception.FibonacciRestException;
import com.jdelgadom.fibonacci.service.FibonacciRetriever;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;

public class FibonacciApiTest {


  @Mock
  private FibonacciRetriever iterativeFibonacciRetriever;

  @Mock
  private FibonacciRetriever recursiveFibonacciRetriever;

  private FibonacciApi fibonacciApi;


  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    fibonacciApi = new FibonacciApi(iterativeFibonacciRetriever, recursiveFibonacciRetriever);
  }

  @Test
  public void fibonacciCalculator() throws Exception {
    Long posicion = Long.parseLong("2");
    Mockito.when(iterativeFibonacciRetriever.fibonacciCalculator(posicion))
        .thenReturn(Long.parseLong("3"));
    assertEquals(200, fibonacciApi.fibonacciCalculator(Long.parseLong("2")).getStatusCode().value());
    assertEquals(3, fibonacciApi.fibonacciCalculator(Long.parseLong("2")).getBody().longValue());
  }

  @Test(expected = FibonacciRestException.class)
  public void fibonacciCalculatorException() {
    fibonacciApi.fibonacciCalculator(Long.parseLong("-1"));
  }

  @Test
  public void fibonacciCalculatorRecursive() {
    Long posicion = Long.parseLong("2");
    Mockito.when(recursiveFibonacciRetriever.fibonacciCalculator(posicion))
        .thenReturn(Long.parseLong("3"));
    assertEquals(200, fibonacciApi.fibonacciCalculatorRecursive(Long.parseLong("2")).getStatusCode().value());
    assertEquals(3, fibonacciApi.fibonacciCalculatorRecursive(Long.parseLong("2")).getBody().longValue());
  }

  @Test(expected = FibonacciRestException.class)
  public void fibonacciCalculatorRecursiveException() {
    fibonacciApi.fibonacciCalculatorRecursive(Long.parseLong("-1"));
  }
}