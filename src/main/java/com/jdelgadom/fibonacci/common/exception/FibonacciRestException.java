package com.jdelgadom.fibonacci.common.exception;

/**
 * Excepción a utilizar cuando hay un error controlado.
 * <p>
 */
public class FibonacciRestException extends RuntimeException {

  /**
   * Identifica la clase cuando es serializada
   */
  private static final long serialVersionUID = 8206218583051711321L;

  /**
   * Construye una nueva excepción con el mensaje especificado.
   *
   * @param mensaje Mensaje de la excepción.
   */
  public FibonacciRestException(String mensaje) {
    super(mensaje);
  }

}
