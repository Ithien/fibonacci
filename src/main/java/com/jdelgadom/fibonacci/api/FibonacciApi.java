package com.jdelgadom.fibonacci.api;

import com.jdelgadom.fibonacci.common.exception.FibonacciRestException;
import com.jdelgadom.fibonacci.service.FibonacciRetriever;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public final class FibonacciApi {



  FibonacciRetriever iterativeFibonacciRetriever;

  FibonacciRetriever recursiveFibonacciRetriver;

  String messageApiError = "Error al llamar al API";

  @Autowired
  public FibonacciApi(@Qualifier("iterativeFibonacciRetriver") FibonacciRetriever iterativeFibonacciRetriever,
                      @Qualifier("recursiveFibonacciRetriver") FibonacciRetriever recursiveFibonacciRetriver) {
    this.iterativeFibonacciRetriever = iterativeFibonacciRetriever;
    this.recursiveFibonacciRetriver = recursiveFibonacciRetriver;

  }

  @ApiOperation(value = "Calcula número fibonacci", response = Long.class)
  @GetMapping(value = "/api/v1/fibonacciCalculator")
  public ResponseEntity<Long> fibonacciCalculator(@RequestParam(value = "position") Long position) {
    try {
      if (position < 0) {
        messageApiError = "El valor de la position debe ser mayor que 0";
        throw new FibonacciRestException(messageApiError);
      }
      return ResponseEntity.ok().body(iterativeFibonacciRetriever.fibonacciCalculator(position));
    } catch (Exception ex) {
      throw new FibonacciRestException(messageApiError);
    }
  }

  @ApiOperation(value = "Calcula número fibonacci con recursividad", response = Long.class)
  @GetMapping(value = "/api/v2/fibonacciCalculator")
  public ResponseEntity<Long> fibonacciCalculatorRecursive(@RequestParam(value = "position") Long position) {
    try {
      if (position < 0) {
        messageApiError = "El valor de la position debe ser mayor que 0";
        throw new FibonacciRestException(messageApiError);
      }
      return ResponseEntity.ok().body(recursiveFibonacciRetriver.fibonacciCalculator(position));
    } catch (Exception ex) {
      throw new FibonacciRestException(messageApiError);
    }
  }

}
