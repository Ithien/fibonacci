package com.jdelgadom.fibonacci;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Created by @jdelgadomolto on 07/11/2020.
 */
//Se activa Swagger para hacer pruebas de web.
@Configuration
@EnableSwagger2
public class SwaggerConfig {


  //Se arranca el API de swagger para poder probar.
  @Bean
  public Docket swaggerApi1() {
    return new Docket(DocumentationType.SWAGGER_2)
        .groupName("api version 1")
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.jdelgadom.fibonacci"))
        .paths(regex("/api/v1.*"))
        .build()
        .apiInfo(apiInfo("version 1.0"));
  }

  @Bean
  public Docket swaggerApi2() {
    return new Docket(DocumentationType.SWAGGER_2)
        .groupName("api version 2")
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.jdelgadom.fibonacci"))
        .paths(regex("/api/v2.*"))
        .build()
        .apiInfo(apiInfo("version 2.0"));
  }

  //Customización de la API
  //Se puedo ir modificando la información y los estilos mostrados en la web.
  //Con esto configuramos el Api para realizar pruebas desde un UX generado
  private ApiInfo apiInfo(String version) {
    return new ApiInfo(
        "Api Fibonacci",
        "Api de REST Controller",
        version,
        "Terms of service",
        new Contact("Jose Delgado", "", "demojo08@gva.es"),
        "License of API", "API license URL", Collections.emptyList());
  }


}