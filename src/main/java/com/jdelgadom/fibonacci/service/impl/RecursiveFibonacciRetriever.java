package com.jdelgadom.fibonacci.service.impl;

import com.jdelgadom.fibonacci.common.exception.FibonacciException;
import com.jdelgadom.fibonacci.service.FibonacciRetriever;
import org.springframework.stereotype.Service;

@Service(value = "recursiveFibonacciRetriver")
public class RecursiveFibonacciRetriever implements FibonacciRetriever {

  public static final String MESSAGE_ERROR_MINOR = "La posición no puede ser menor que 0 ni mayor que ";
  private Long max = (Long.MAX_VALUE/2);

  @Override
  public long fibonacciCalculator(Long position) {

    if (position < 0 || position > max){
      throw new FibonacciException(MESSAGE_ERROR_MINOR + max);
    }

    long resultado = 0;

    for (int i = 0; i <= position; i++) {
      resultado = funcionFibonacci(i);
    }

    return resultado;
  }

  private long funcionFibonacci(long numero) {
    if (numero < 2) {
      return numero;
    } else {
      return funcionFibonacci(numero - 1) + funcionFibonacci(numero - 2);
    }
  }
}
