package com.jdelgadom.fibonacci.service.impl;

import com.jdelgadom.fibonacci.common.exception.FibonacciException;
import com.jdelgadom.fibonacci.service.FibonacciRetriever;
import org.springframework.stereotype.Service;

@Service(value = "iterativeFibonacciRetriver")
public class IterativeFibonacciRetriever implements FibonacciRetriever {

  public static final String MESSAGE_ERROR_MINOR = "La posición no puede ser menor que 0 ni mayor que ";
  private Long max = (Long.MAX_VALUE/2);

  @Override
  public long fibonacciCalculator(Long position) {

    if (position < 0 || position > max){
      throw new FibonacciException(MESSAGE_ERROR_MINOR + max);
    }
    
    long next = 1;
    long actual = 0;
    long auxiliar = 0;
    
    for (long x = 1; x <= position; x++) {
      auxiliar = actual;
      actual = next;
      next = next + auxiliar;
    }

    //3- Se devuelve el resultado
    return actual;
  }
}
