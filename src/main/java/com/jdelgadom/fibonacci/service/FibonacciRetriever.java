package com.jdelgadom.fibonacci.service;

public interface FibonacciRetriever {

  /**
   * @param position = position de la serie fibonacci que se desea devolver
   * @return Devuelve la posición de la serie fibonacci
   * @Author jdelgadomolto
   */
  long fibonacciCalculator(Long position);

}
